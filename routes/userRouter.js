const pool = require('../db');
const express = require("express");
const router = express.Router();

router.get("/accounts", (req, res) => {
	let query = `select username, email, created_at from accounts`;
	pool.query(query, (err, response) => {
		if (err) {
			res.status(500).json({
				message: "Error getting accounts! ",
				err,
			});
		} else {
			res.status(200).json({
				data: response.rows,
			});
		}
	});
});

router.post("/accounts", (req, res) => {
	let query = `
          INSERT INTO accounts
          (username, password, email)
          VALUES
          ($1, $2, $3)
     `;
	let values = [req.body.username, req.body.password, req.body.email];
	pool.query(query, values, (err, _) => {
		if (err) {
			res.status(500).json({
				message: "Error inserting account, please try again ",
				err,
			});
		} else {
			res.status(200).json({
				message: "succesfully created new user",
			});
		}
	});
});

router.put('/accounts/:id', (req, res) => {
     let query = `update accounts set email = $1, password = $2 where id = $3`;
     let values = [ req.body.email, req.body.password, req.params.id];

     pool.query(query, values, (err, _) => {
          if (err) {
               res.status(500).json({
                    message: "Error updating account, please try again ",
                    err,
               });
          } else {
               res.status(200).json({
                    message: "successfully updating accounts!",
               })
          }
     })
});

module.exports = router;
