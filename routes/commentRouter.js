const express = require('express');
const router = express.Router();
const pool = require('../db');

router.post('/comments', (req, res) => {
     let query = 'insert into comments (article_id, user_id, comment) values ($1, $2, $3)';
     let values = [req.body.article_id, req.body.user_id, req.body.comment];
     pool.query(query, values, (err, response) => {
          if (err) {
               res.status(500).json({
                    message: "Error inserting comments! Please try again",
                    err,
               });
          } else {
               res.status(200).json({
                    message: "Comment added!",
               });
          }
     });
});

router.put('/comments', (req, res) => {
     let query = 'update comments set comment = $1, edited_at = CURRENT_TIMESTAMP where comment_id = $2';
     // const time = new Date();
     let values = [req.body.comment, req.body.comment_id];
     pool.query(query, values, (err, response) => {
          if (err) {
               res.status(500).json({
                    message: "Error updating comments! Please try again",
                    err,
               });
          } else {
               res.status(200).json({
                    message: "comment updated!"
               });
          }
     });
});

router.delete('/comments/:id', (req, res) => {
     let query = `delete from comments where comment_id = ${req.params.id}`;
     pool.query(query, (err, response) => {
          if (err) {
               res.status(500).json({
                    message: "Error deleting comments! Please try again",
                    err,
               });
          } else {
               res.status(200).json({
                    message: "Successfully delete comment!"
               })
          }
     });
});

module.exports = router;