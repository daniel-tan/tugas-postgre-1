const express = require('express');
const router = express.Router();
const pool = require('../db');

router.get("/articles/:id", (req, res) => {
	// let query = `
	// 	SELECT
	// 	articles.id, articles.title, articles.body, accounts.username as author, accounts.email as contact_author
	// 	FROM articles
	// 	JOIN accounts
	// 	ON articles.user_id = accounts.id
	// 	where articles.id = ${req.params.id} and articles.deleted_at is null;  
	// `;
	
	let query = 
	`select articles.id, articles.title, articles.body, accounts.username as author, accounts.email as contact_author, comments.comment, comments.created_at
	from comments
	inner join articles on articles.id = comments.article_id
	inner join accounts on comments.user_id = accounts.id
	where articles.id = ${req.params.id} and articles.deleted_at is null`;

	// let query2 = `
	// 	select accounts.username, comments.comment, comments.created_at, comments.edited_at 
	// 	from comments
	// 	inner join articles on articles.id = comments.article_id
	// 	inner join accounts on comments.user_id = accounts.id
	// 	where articles.id = ${req.params.id} and articles.deleted_at is null`;

	pool.query(query, (err, response1) => {
		if (err) {
			res.status(500).json({
				message: "Error can't find articles!",
				err,
			});
		} else {
			// pool.query(query2, (err, response2) => {
			// 	if (err) {
			// 		res.status(500).json({
			// 			message: "Error can't find articles!",
			// 			err,
			// 		});
			// 	} else {
			// 		res.status(200).json({
			// 			data: response1.rows,
			// 			comments: response2.rows,
			// 		});
			// 	}
			// });
			res.status(200).json({
				data: response1.rows,
				comments: response1.rows['comment'],
			});
		}
	});
});

router.get("/articles", (req, res) => {
	let query = `
  SELECT
  articles.id, articles.title, articles.body, accounts.username as author, accounts.email as contact_author
  FROM articles
    JOIN accounts
        ON articles.user_id = accounts.id
  WHERE articles.deleted_at is null
  ORDER BY id ASC
  ;  
  `;
	pool.query(query, (err, response) => {
		if (err) {
			res.status(500).json({
				message: "Some error happen",
				err,
			});
		} else {
			res.status(200).json({
				data: response.rows,
				message: "Get All Articles",
			});
		}
	});
});


router.post("/articles", (req, res) => {
	let query = `
  INSERT INTO articles
  (title, body, user_id)
  VALUES
  ($1, $2, $3)
  `;
	let values = [req.body.title, req.body.body, req.body.user_id];
	pool.query(query, values, (err, _) => {
		if (err) {
			res.status(500).json({
				message: "Some error happen",
				err,
			});
		} else {
			res.status(200).json({
				message: "Succesfully created new article",
			});
		}
	});
});

router.put("/articles/:id", (req, res) => {
	let query = `
  UPDATE articles
  SET 
  title = $1,
  body = $2,
  updated_at = $3
  where id = $4;
  `;
	let values = [req.body.title, req.body.body, new Date(), req.params.id];
	pool.query(query, values, (err, _) => {
		if (err) {
			res.status(500).json({
				message: "Some error happen",
				err,
			});
		} else {
			res.status(200).json({
				message: "succesfully update data",
			});
		}
	});
});

router.delete("/articles/:id", (req, res) => {
	let query = `
  UPDATE articles
  SET
  deleted_at = $1
  where id = $2;
  `;
	let values = [new Date(), req.params.id];
	pool.query(query, values, (err, _) => {
		if (err) {
			res.status(500).json({
				message: "Some error happen",
				err,
			});
		} else {
			res.status(200).json({
				message: "succesfully update data",
			});
		}
	});
});

module.exports = router;