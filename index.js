const express = require("express");
const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const userRoutes = require("./routes/userRouter");
const articleRoutes = require("./routes/articleRouter");
const commentRoutes = require("./routes/commentRouter");
app.use(userRoutes);
app.use(articleRoutes);
app.use(commentRoutes);

app.get("/", (req, res) => {
	//   pool.query("SELECT * from accounts", (err, response) => {
	//     console.log(response.rows);
	//   });
	pool.query("select * from accounts", (err, response) => {
		console.log("er", err);
		if (!err) {
			res.json({
				data: response.rows,
			});
		} else {
			res.json({
				err,
				message: "error",
			});
		}
	});
});

app.listen(3000);
